/*
 * Proyectil.cpp
 *
 *  Created on: 23/02/2014
 *      Author: Sancho
 */

#include "Proyectil.h"


Proyectil::Proyectil(int anchura_mapa, int altura_mapa, std::string imgPath, Personaje lanzador, int width, int height, int incWidth,int incH, int spritesPorLinea, float velocidad) {
	this->altura_mapa = altura_mapa;
	this->anchura_mapa = anchura_mapa;
	spritesheet = imgPath;
	spriteSelector.top = lanzador.getUltimaDireccion() * incH;
	spriteSelector.left = 0;
	spriteSelector.height = height;
	spriteSelector.width = width;
	this->incWidth = incWidth;
	texture.loadFromFile(imgPath, spriteSelector);
	direccion = lanzador.getUltimaDireccion();
	vecesUltimoSprite = 0;
	this->velocidad = velocidad;
	this->spritesPorLinea = spritesPorLinea;
	sprite.setTexture(texture);
	sprite.move(this->posicionInicial(lanzador));
	this->velocidad = velocidad;
}

Proyectil::~Proyectil() {

}

void Proyectil::calcularSprite(){
	spriteSelector.left = incWidth * (vecesUltimoSprite%spritesPorLinea);
	vecesUltimoSprite++;
	texture.loadFromFile(spritesheet, spriteSelector);
	sprite.setTexture(texture);
}

void Proyectil::mover() {
	calcularSprite();
	sf::Vector2f posicion(0.f, 0.f);
	switch(direccion){
	case 0:
		posicion.y += velocidad;
		break;
	case 1:
		posicion.x -= velocidad;
		break;
	case 2:
		posicion.x += velocidad;
		break;
	case 3:
		posicion.y -= velocidad;
		break;
	}
	sprite.move(posicion);
}

bool Proyectil::seSale() {
	bool sesale = false;

	if(sprite.getPosition().y>altura_mapa || sprite.getPosition().x>anchura_mapa || sprite.getPosition().y<0 || sprite.getPosition().x<0)
		sesale = true;
	return sesale;
}

sf::Vector2f Proyectil::posicionInicial(Personaje lanzador) {
	sf::Vector2f posicion(0.f, 0.f);
	posicion.y = lanzador.getSpriteJugador().getPosition().y + 15;
	posicion.x = lanzador.getSpriteJugador().getPosition().x + 5;

	return posicion;
}



int Proyectil::getAlturaMapa() const {
	return altura_mapa;
}

void Proyectil::setAlturaMapa(int alturaMapa) {
	altura_mapa = alturaMapa;
}

int Proyectil::getAnchuraMapa() const {
	return anchura_mapa;
}

void Proyectil::setAnchuraMapa(int anchuraMapa) {
	anchura_mapa = anchuraMapa;
}

int Proyectil::getDireccion() const {
	return direccion;
}

void Proyectil::setDireccion(int direccion) {
	this->direccion = direccion;
}

int Proyectil::getIncWidth() const {
	return incWidth;
}

void Proyectil::setIncWidth(int incWidth) {
	this->incWidth = incWidth;
}

const sf::Sprite& Proyectil::getSprite() const {
	return sprite;
}

void Proyectil::setSprite(const sf::Sprite& sprite) {
	this->sprite = sprite;
}

const sf::IntRect& Proyectil::getSpriteSelector() const {
	return spriteSelector;
}

void Proyectil::setSpriteSelector(const sf::IntRect& spriteSelector) {
	this->spriteSelector = spriteSelector;
}

const std::string& Proyectil::getSpritesheet() const {
	return spritesheet;
}

void Proyectil::setSpritesheet(const std::string& spritesheet) {
	this->spritesheet = spritesheet;
}

int Proyectil::getSpritesPorLinea() const {
	return spritesPorLinea;
}

void Proyectil::setSpritesPorLinea(int spritesPorLinea) {
	this->spritesPorLinea = spritesPorLinea;
}

const sf::Texture& Proyectil::getTexture() const {
	return texture;
}

void Proyectil::setTexture(const sf::Texture& texture) {
	this->texture = texture;
}

int Proyectil::getVecesUltimoSprite() const {
	return vecesUltimoSprite;
}

void Proyectil::setVecesUltimoSprite(int vecesUltimoSprite) {
	this->vecesUltimoSprite = vecesUltimoSprite;
}

float Proyectil::getVelocidad() const {
	return velocidad;
}

void Proyectil::setVelocidad(float velocidad) {
	this->velocidad = velocidad;
}

