/*
 * Personaje.cpp
 *
 *  Created on: 11/03/2014
 *      Author: Sancho
 */

#include "Personaje.h"
Personaje::Personaje(sf::Vector2f posicion, std::string spritesheet, float velocidad,
				int escala_top, int escala_left, int altura, int anchura,
				int spritesPorLinea){
	this->posicion = posicion;
	this->spritesheet = spritesheet;
	this->velocidad = velocidad;
	this->escala_top = escala_top;
	this->escala_left = escala_left;
	this->spritesPorLinea = spritesPorLinea;
	this->spriteSelector.height = altura;
	this->spriteSelector.width = anchura;
	this->vecesUltimaDireccion = 0;
	this->ultimaDireccion = 0;
	this->calcularSprite();
	this->vida = 0;
	this->spriteJugador.move(posicion);
}

void Personaje::calcularOrientacion(int ultimoEvento){
	if(ultimoEvento == ultimaDireccion)
		vecesUltimaDireccion++;
	else{
		ultimaDireccion = ultimoEvento;
		vecesUltimaDireccion = 0;
	}

}

void Personaje::calcularSprite(){
	spriteSelector.top = ultimaDireccion * escala_top;
	spriteSelector.left = escala_left * (vecesUltimaDireccion%spritesPorLinea);
	texturaJugador.loadFromFile(spritesheet, spriteSelector);
	spriteJugador.setTexture(texturaJugador);
}

int Personaje::getEscalaLeft()  {
	return escala_left;
}

void Personaje::setEscalaLeft(int escalaLeft) {
	escala_left = escalaLeft;
}

int Personaje::getEscalaTop()  {
	return escala_top;
}

void Personaje::setEscalaTop(int escalaTop) {
	escala_top = escalaTop;
}

 sf::Vector2f& Personaje::getPosicion()  {
	return posicion;
}

void Personaje::setPosicion( sf::Vector2f& posicion) {
	this->posicion = posicion;
}



 sf::IntRect& Personaje::getSpriteSelector()  {
	return spriteSelector;
}

void Personaje::setSpriteSelector( sf::IntRect& spriteSelector) {
	this->spriteSelector = spriteSelector;
}

 std::string& Personaje::getSpritesheet()  {
	return spritesheet;
}

void Personaje::setSpritesheet( std::string& spritesheet) {
	this->spritesheet = spritesheet;
}

int Personaje::getSpritesPorLinea()  {
	return spritesPorLinea;
}

void Personaje::setSpritesPorLinea(int spritesPorLinea) {
	this->spritesPorLinea = spritesPorLinea;
}

 sf::Texture& Personaje::getTexturaJugador()  {
	return texturaJugador;
}

void Personaje::setTexturaJugador( sf::Texture& texturaJugador) {
	this->texturaJugador = texturaJugador;
}

int Personaje::getUltimaDireccion()  {
	return ultimaDireccion;
}

void Personaje::setUltimaDireccion(int ultimaDireccion) {
	this->ultimaDireccion = ultimaDireccion;
}

int Personaje::getVecesUltimaDireccion()  {
	return vecesUltimaDireccion;
}

void Personaje::setVecesUltimaDireccion(int vecesUltimaDireccion) {
	this->vecesUltimaDireccion = vecesUltimaDireccion;
}

float Personaje::getVelocidad()  {
	return velocidad;
}


const sf::Sprite& Personaje::getSpriteJugador() const {
	return spriteJugador;
}

int Personaje::getVida() const {
	return vida;
}



void Personaje::setSpriteJugador(const sf::Sprite& spriteJugador) {
	this->spriteJugador = spriteJugador;
}

void Personaje::setVida(int vida) {
	this->vida = vida;
}

void Personaje::setVelocidad(float velocidad) {
	this->velocidad = velocidad;
}

Personaje::~Personaje(){

}
