/*
 zz	z* Game.h
 *
 *  Created on: 04/02/2014
 *      Author: Sancho
 */

#ifndef GAME_H_
#define GAME_H_
#include <iostream>
#include <list>
#include "Jugador.h"
#include "Proyectil.h"
#include "Mob.h"
#include <sstream>
#include <stdlib.h>
#include "Collision.h"
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

class Game {
public:
	Game();
	void run();
	~Game();
private:
	void drawProyectiles();
	void processEvents();
	void handlePlayerInput(sf::Keyboard::Key key, bool isPressed);
	void update(sf::Time dt);
	void render();
	void genMob();
	void drawMobs();
	void turnoMobs(Jugador &j, sf::Time time);
	void colisionMobsProyectiles();
	sf::Vector2f posicionMob();
private:
	sf::RenderWindow mWindow;
	sf::Time Cooldown;
	sf::Time genMobTimer;
	sf::SoundBuffer fbuffer;
	sf::Sound fbsound;
	sf::SoundBuffer sbuffer;
	sf::Sound ssound;
	Jugador myr;
	std::list<Proyectil> proyectiles;
	std::list<Mob> enemigos;
	sf::IntRect spriteSelectorPet;
	sf::Sprite pet;
	sf::Texture backGround;
	sf::Texture petText;
	sf::Sprite fondo;
	bool mIsMovingUp, mIsMovingDown, mIsMovingLeft, mIsMovingRight;
	sf::SoundBuffer buffer;
	sf::Sound sound;
	bool gameOver;
	int puntos;
};

#endif
