/*
 * Game.cpp
 *
 *  Created on: 04/02/2014
 *      Author: Sancho
 */

#include "Game.h"

Game::Game() :
		mWindow(sf::VideoMode(600, 480), "Juego"), myr(sf::Vector2f(0.f, 0.f),
				std::string("personaje.png"), 5.f, 49, 32, 46, 32, 4), Cooldown(
				sf::seconds(0.f)), genMobTimer(sf::seconds(2.f)) {
	sf::Vector2f movement(0.f, 0.f);
	backGround.loadFromFile("fondo.png");
	fondo.setTexture(backGround);
	mWindow.setFramerateLimit(30);
	mIsMovingUp = mIsMovingDown = mIsMovingLeft = mIsMovingRight = false;
	myr.movimiento(0);
	mWindow.draw(myr.getSpriteJugador());
	buffer.loadFromFile("ambiental.ogg");
	fbuffer.loadFromFile("fb.ogg");
	sbuffer.loadFromFile("skeldead.ogg");
	ssound.setBuffer(sbuffer);
	fbsound.setBuffer(fbuffer);
	fbsound.setVolume(20.f);
	sound.setBuffer(buffer);
	sound.setLoop(true);
	sound.play();
	gameOver = false;
	puntos = 0;
	srand (time(NULL));
}

void Game::run() {
	sf::Clock clock;
	while (mWindow.isOpen()) {
		sf::Time deltaTime = clock.restart();
		Cooldown = Cooldown - deltaTime;
		genMobTimer = genMobTimer - deltaTime;
		processEvents();
		update(deltaTime);
		render();
	}
}

void Game::processEvents() {
	sf::Event event;
	while (mWindow.pollEvent(event)) {
		switch (event.type) {
		//Si se ha pulsado o soltado una tecla, mandamos a handlePlayerInput la informacion
		case sf::Event::KeyPressed:
			handlePlayerInput(event.key.code, true);
			break;
		case sf::Event::KeyReleased:
			handlePlayerInput(event.key.code, false);
			break;
		case sf::Event::Closed:
			mWindow.close();
			break;
		}
	}
}

void Game::handlePlayerInput(sf::Keyboard::Key key, bool isPressed) {
	if (key == sf::Keyboard::W || key == sf::Keyboard::Up)
		mIsMovingUp = isPressed;
	else if (key == sf::Keyboard::S || key == sf::Keyboard::Down)
		mIsMovingDown = isPressed;
	else if (key == sf::Keyboard::A || key == sf::Keyboard::Left)
		mIsMovingLeft = isPressed;
	else if (key == sf::Keyboard::D || key == sf::Keyboard::Right)
		mIsMovingRight = isPressed;
	else if ((key == sf::Keyboard::Space || key == sf::Keyboard::F)) {
		if (Cooldown <= sf::Time::Zero &&!gameOver) {
			proyectiles.push_front(
					Proyectil(600, 480, "fireball.png", myr, 25, 25, 35, 33, 3,
							15.0f));
			fbsound.play();
			Cooldown = sf::seconds(0.2f);
		}

	}
}

void Game::update(sf::Time dt) {
if(!gameOver){
	if (mIsMovingUp) {
		myr.movimiento(3);
	}
	if (mIsMovingDown) {
		myr.movimiento(0);

	}
	if (mIsMovingLeft && !mIsMovingDown && !mIsMovingUp) {
		myr.movimiento(1);

	}
	if (mIsMovingRight && !mIsMovingDown && !mIsMovingUp) {
		myr.movimiento(2);

	}

	if (myr.getVida() <= 0) {
		gameOver = true;
	}
	else{
		genMob();
		turnoMobs(myr,dt);
		colisionMobsProyectiles();
}
}
}

void Game::drawProyectiles() {
	if (!proyectiles.empty()) {
		for (std::list<Proyectil>::iterator iterator = proyectiles.begin(),
				end = proyectiles.end(); iterator != end; ++iterator) {
			if (!iterator->seSale()) {
				iterator->mover();
				mWindow.draw(iterator->getSprite());
			} else {
				iterator = proyectiles.erase(iterator);
			}
		}
	}
}

void Game::render() {
	mWindow.clear();
	int tam = 10 * myr.getVida();
	sf::Font f;
	f.loadFromFile("Yorktown.ttf");
	std::string s = "Puntuacion:";
	std::ostringstream os;
	os << puntos;
	s = s + os.str();

	sf::Text t(s, f);
	t.setCharacterSize(15);
	t.move(400.f, 0.f);

	sf::RectangleShape rectangle(sf::Vector2f(tam, 20));
	rectangle.setFillColor(sf::Color::Green);
	mWindow.draw(fondo);
	rectangle.move(sf::Vector2f(250.f, 0.f));
	mWindow.draw(myr.getSpriteJugador());
	mWindow.draw(t);
	mWindow.draw(rectangle);
	drawMobs();
	drawProyectiles();
	if (gameOver) {
		sf::Font f;
		f.loadFromFile("Yorktown.ttf");
		sf::Text t("Game Over", f);
		t.setCharacterSize(60);
		t.move(150.f, 140.f);
		t.setColor(sf::Color::Red);
		mWindow.draw(t);

	}
	mWindow.display();

}

Game::~Game() {
	mWindow.~Window();
	fondo.~Sprite();
	backGround.~Texture();

}

void Game::genMob() {
	if (genMobTimer <= sf::Time::Zero) {
		Mob nuevo(posicionMob(), std::string("skeleton.png"), 3.f,49, 32, 46, 32, 4);
		enemigos.push_front(nuevo);
		genMobTimer = sf::seconds(2.f);
	}
}

sf::Vector2f Game::posicionMob(){
	sf::Vector2f posicion(0.f,0.f);
	int opcion = rand() % 4 +1;

	switch (opcion) {
	case 1:
		posicion.x = 0.f;
		posicion.y = 240.f;
		break;
	case 2:
		posicion.x = 300.f;
		posicion.y = 0.f;
		break;
	case 3:
		posicion.x = 600.f;
		posicion.y = 240.f;
		break;
	case 4:
		posicion.x = 300.f;
		posicion.y = 480.f;
		break;
	}


	return posicion;
}

void Game::drawMobs() {
	if (!enemigos.empty()) {
			for (std::list<Mob>::iterator iterator = enemigos.begin(),
					end = enemigos.end(); iterator != end; ++iterator) {
					mWindow.draw(iterator->getSpriteJugador());
			}
		}
}

void Game::turnoMobs(Jugador& j, sf::Time time) {
	if (!enemigos.empty()) {
				for (std::list<Mob>::iterator iterator = enemigos.begin(),
						end = enemigos.end(); iterator != end; ++iterator) {
						iterator->turno(j, time);
				}
			}
}

void Game::colisionMobsProyectiles() {
	if (!enemigos.empty() && !proyectiles.empty()) {
					for (std::list<Mob>::iterator iterator = enemigos.begin(),
							end = enemigos.end(); iterator != end; ++iterator) {
						for (std::list<Proyectil>::iterator iteratorP = proyectiles.begin(),
													end = proyectiles.end(); iteratorP != end; ++iteratorP) {
												if(Collision::CircleTest(iterator->getSpriteJugador(), iteratorP->getSprite())){
													iterator = enemigos.erase(iterator);
													iteratorP = proyectiles.erase(iteratorP);
													puntos += 1;
													ssound.play();

											}

					}
				}
	}

}
