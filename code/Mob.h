/*
 * Mob.h
 *
 *  Created on: 14/04/2014
 *      Author: Sancho
 */
#include <SFML/Graphics.hpp>
#include "Personaje.h"
#include "Jugador.h"
#include "Collision.h"
#include <math.h>
#ifndef MOB_H_
#define MOB_H_

class Mob : public Personaje {
public:
	Mob(sf::Vector2f posicion, std::string spritesheet, float velocidad,
			int escala_top, int escala_left, int altura, int anchura,
			int spritesPorLinea);
	void movimiento(Jugador jugador);
	void atacar(Jugador &jugador);
	void turno(Jugador &jugador, sf::Time dt);
	bool inRange(Jugador jugador);
	void calcularOrientacion(int ultimoEvento, bool si);
	virtual ~Mob();

private:
	sf::Time cooldown;
};

#endif /* MOB_H_ */
