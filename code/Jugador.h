#include <SFML/Graphics.hpp>
#include "Personaje.h"
#ifndef JUGADOR_H_
#define JUGADOR_H_

class Jugador : public Personaje {
public:
	Jugador(sf::Vector2f posicion, std::string spritesheet, float velocidad,
			int escala_top, int escala_left, int altura, int anchura,
			int spritesPorLinea);
	int getUltimaDireccion();
	void setUltimaDireccion(int nueva);
	int getVecesUltimaDireccion();
	void setVecesUltimaDireccion(int nueva);
	float getVelocidad();
	void setVelocidad(float nueva);
	void calcularOrientacion(int ultimoEvento);
	void calcularSprite();
	void movimiento(int ultimo_evento);
	virtual ~Jugador();
	sf::Vector2f calcularRuta();


};

#endif /* Jugador_H_ */
