/*
 * Mob.cpp
 *
 *  Created on: 14/04/2014
 *      Author: Sancho
 */

#include "Mob.h"

Mob::Mob(sf::Vector2f posicion, std::string spritesheet, float velocidad,
		int escala_top, int escala_left, int altura, int anchura,
		int spritesPorLinea) :
		Personaje(posicion, spritesheet, velocidad, escala_top, escala_left,
				altura, anchura, spritesPorLinea),cooldown(sf::seconds(1.f)) {
	this->setVida(3);
}

void Mob::movimiento(Jugador jugador) {
	sf::Vector2f ruta(0.f, 0.f);
	bool quieto = true;
	float x = jugador.getSpriteJugador().getPosition().x;
	float y = jugador.getSpriteJugador().getPosition().y;
	float xS = this->getSpriteJugador().getPosition().x;
	float yS = this->getSpriteJugador().getPosition().y;
	int ultimoEvento = ultimaDireccion;

if(!Collision::PixelPerfectTest(jugador.getSpriteJugador(), this->getSpriteJugador())){
	if (abs(x-xS)<=abs(y-yS)+velocidad && abs(y-yS) > velocidad ) { //Mover de arriba a abajo
		if (this->getSpriteJugador().getPosition().y > y) { //Esta por debajo del personaje
			ruta.y -= velocidad;
			ultimoEvento = 3;
			quieto = false;

		} else if (this->getSpriteJugador().getPosition().y <= y) {
			ruta.y += velocidad;
			ultimoEvento = 0;
			quieto = false;
		}
	}
	if (abs(x-xS)>abs(y-yS)&& abs(x-xS) != 0  ) {	//Mover de izquierda a derecha
		if (this->getSpriteJugador().getPosition().x > x) {	//Est� a la derecha del personaje
			ruta.x -= velocidad;
			ultimoEvento = 1;
			quieto = false;
		} else if(this->getSpriteJugador().getPosition().x <= x)  {	
			ruta.x += velocidad;
			ultimoEvento = 2;
			quieto = false;

		}
	}
}
	calcularOrientacion(ultimoEvento, quieto);
	calcularSprite();
	spriteJugador.move(ruta);


}

void Mob::calcularOrientacion(int ultimoEvento, bool quieto) {
	if (ultimoEvento == ultimaDireccion && !quieto)
		vecesUltimaDireccion++;
	else {
		ultimaDireccion = ultimoEvento;
		vecesUltimaDireccion = 0;
	}

}

void Mob::atacar(Jugador &jugador) {
	if(inRange(jugador) && cooldown <= sf::Time::Zero && jugador.getVida()>0){
		int nueva = jugador.getVida()-1;
		jugador.setVida(nueva);

		cooldown = sf::seconds(1.0f);
	}


}

void Mob::turno(Jugador &jugador, sf::Time dt) {
	movimiento(jugador);
	atacar(jugador);
	cooldown = cooldown - dt;

}

Mob::~Mob() {

}

bool Mob::inRange(Jugador jugador) {

	return Collision::PixelPerfectTest(this->getSpriteJugador(), jugador.getSpriteJugador());


}

