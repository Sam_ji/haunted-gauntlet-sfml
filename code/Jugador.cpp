/*
 * Jugador.cpp
 *
 *  Created on: 06/02/2014
 *      Author: Sancho
 */

#include "Jugador.h"
Jugador::Jugador(sf::Vector2f posicion, std::string spritesheet,
		float velocidad, int escala_top, int escala_left, int altura,
		int anchura, int spritesPorLinea):Personaje(posicion, spritesheet, velocidad,escala_top,escala_left,altura,anchura,spritesPorLinea){
		this->setVida(10);



}


void Jugador::calcularOrientacion(int ultimoEvento){
	if(ultimoEvento == ultimaDireccion)
		vecesUltimaDireccion++;
	else{
		ultimaDireccion = ultimoEvento;
		vecesUltimaDireccion = 0;
	}

}

void Jugador::calcularSprite(){
	spriteSelector.top = ultimaDireccion * escala_top;
	spriteSelector.left = escala_left * (vecesUltimaDireccion%spritesPorLinea);
	texturaJugador.loadFromFile(spritesheet, spriteSelector);
	spriteJugador.setTexture(texturaJugador);
}

sf::Vector2f Jugador::calcularRuta(){
	sf::Vector2f ruta(0.f, 0.f);
	switch(ultimaDireccion){
		case 0:
			if(spriteJugador.getPosition().y<435)
			ruta.y += velocidad;
			break;
		case 1:
			if(spriteJugador.getPosition().x>0)
			ruta.x -= velocidad;
			break;
		case 2:
			if(spriteJugador.getPosition().x<565)
			ruta.x += velocidad;
			break;
		case 3:
			if(spriteJugador.getPosition().y>0)
			ruta.y -= velocidad;
			break;
	}
	return ruta;
}

void Jugador::movimiento(int ultimo_evento) {
	calcularOrientacion(ultimo_evento);
	calcularSprite();
	spriteJugador.move(calcularRuta());
}



Jugador::~Jugador(){

}



////Getters & Setters
//int Jugador::getEscalaLeft() const {
//	return escala_left;
//}
//
//void Jugador::setEscalaLeft(int escalaLeft) {
//	escala_left = escalaLeft;
//}
//
//int Jugador::getEscalaTop() const {
//	return escala_top;
//}
//
//void Jugador::setEscalaTop(int escalaTop) {
//	escala_top = escalaTop;
//}
//
//const sf::Vector2f& Jugador::getPosicion() const {
//	return posicion;
//}
//
//void Jugador::setPosicion(const sf::Vector2f& posicion) {
//	this->posicion = posicion;
//}
//
//const sf::Sprite& Jugador::getSpriteJugador() const {
//	return spriteJugador;
//}
//
//void Jugador::setSpriteJugador(const sf::Sprite& spriteJugador) {
//	this->spriteJugador = spriteJugador;
//}
//
//const sf::IntRect& Jugador::getSpriteSelector() const {
//	return spriteSelector;
//}
//
//void Jugador::setSpriteSelector(const sf::IntRect& spriteSelector) {
//	this->spriteSelector = spriteSelector;
//}
//
//const std::string& Jugador::getSpritesheet() const {
//	return spritesheet;
//}
//
//void Jugador::setSpritesheet(const std::string& spritesheet) {
//	this->spritesheet = spritesheet;
//}
//
//int Jugador::getSpritesPorLinea() const {
//	return spritesPorLinea;
//}
//
//void Jugador::setSpritesPorLinea(int spritesPorLinea) {
//	this->spritesPorLinea = spritesPorLinea;
//}
//
//const sf::Texture& Jugador::getTexturaJugador() const {
//	return texturaJugador;
//}
//
//
//void Jugador::setTexturaJugador(const sf::Texture& texturaJugador) {
//	this->texturaJugador = texturaJugador;
//}
//
//int Jugador::getUltimaDireccion(){
//	return ultimaDireccion;
//}
//
//int Jugador::getVecesUltimaDireccion(){
//	return vecesUltimaDireccion;
//}
