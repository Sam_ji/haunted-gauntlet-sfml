/*
 * Proyectil.h
 *
 *  Created on: 23/02/2014
 *      Author: Sancho
 */
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Jugador.h"
#ifndef PROYECTIL_H_
#define PROYECTIL_H_

class Proyectil {
public:
	Proyectil(int anchura_mapa, int altura_mapa, std::string imgPath, Personaje lanzador, int width, int height, int incWidth, int inHeight, int spritesPorLinea, float velocidad);
	virtual ~Proyectil();
	void calcularSprite();
	void mover();
	bool seSale();
	//Getters y setters
	int getAlturaMapa() const;
	void setAlturaMapa(int alturaMapa);
	int getAnchuraMapa() const;
	void setAnchuraMapa(int anchuraMapa);
	int getDireccion() const;
	void setDireccion(int direccion);
	int getIncWidth() const;
	void setIncWidth(int incWidth);
	const sf::Sprite& getSprite() const;
	void setSprite(const sf::Sprite& sprite);
	const sf::IntRect& getSpriteSelector() const;
	void setSpriteSelector(const sf::IntRect& spriteSelector);
	const std::string& getSpritesheet() const;
	void setSpritesheet(const std::string& spritesheet);
	int getSpritesPorLinea() const;
	void setSpritesPorLinea(int spritesPorLinea);
	const sf::Texture& getTexture() const;
	void setTexture(const sf::Texture& texture);
	int getVecesUltimoSprite() const;
	void setVecesUltimoSprite(int vecesUltimoSprite);
	float getVelocidad() const;
	void setVelocidad(float velocidad);

private:
	int anchura_mapa;
	int altura_mapa;
	sf::Texture texture;
	sf::Sprite sprite;
	sf::IntRect spriteSelector;
	std::string spritesheet;
	int incWidth;
	float velocidad;
	int direccion;
	int vecesUltimoSprite;
	int spritesPorLinea;
	sf::Vector2f posicionInicial(Personaje lanzador);

};

#endif /* PROYECTIL_H_ */
