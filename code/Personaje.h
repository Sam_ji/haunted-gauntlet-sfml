/*
 * Personaje.h
 *
 *  Created on: 11/03/2014
 *      Author: Sancho
 */
#include <SFML/Graphics.hpp>
#ifndef PERSONAJE_H_
#define PERSONAJE_H_

class Personaje {
protected:
	sf::Sprite spriteJugador;
	sf::Texture texturaJugador;
	sf::IntRect spriteSelector;
	sf::Vector2f posicion;
	std::string spritesheet;
	int ultimaDireccion;
	int vecesUltimaDireccion;
	int escala_top;
	int escala_left;
	int spritesPorLinea;
	float velocidad;
	int vida;

public:
	Personaje(sf::Vector2f posicion, std::string spritesheet, float velocidad,
			int escala_top, int escala_left, int altura, int anchura,
			int spritesPorLinea);
	void calcularOrientacion(int ultimoEvento);
	void calcularSprite();
	void movimiento(int ultimo_evento);
	~Personaje();
	sf::Vector2f calcularRuta();

	//Getters & setters
	int getEscalaLeft() ;
	void setEscalaLeft(int escalaLeft);
	int getEscalaTop() ;
	void setEscalaTop(int escalaTop);
	 sf::Vector2f& getPosicion() ;
	void setPosicion( sf::Vector2f& posicion);

	 sf::IntRect& getSpriteSelector() ;
	void setSpriteSelector( sf::IntRect& spriteSelector);
	 std::string& getSpritesheet() ;
	void setSpritesheet( std::string& spritesheet);
	int getSpritesPorLinea() ;
	void setSpritesPorLinea(int spritesPorLinea);
	 sf::Texture& getTexturaJugador() ;
	void setTexturaJugador( sf::Texture& texturaJugador);
	int getUltimaDireccion() ;
	void setUltimaDireccion(int ultimaDireccion);
	int getVecesUltimaDireccion() ;
	void setVecesUltimaDireccion(int vecesUltimaDireccion);
	float getVelocidad() ;
	void setVelocidad(float velocidad);
	const sf::Sprite& getSpriteJugador() const;
	void setSpriteJugador(const sf::Sprite& spriteJugador);
	int getVida() const;
	void setVida(int vida);
};

#endif /* PERSONAJE_H_ */
